# SRRG_IMAGE_ASSEMBLER

Utility for editing video starting from a gadzilion of images, listed in a single txt file.

Examples are coming...stay Tuned.

## FFMPEG conversion

Once your images have been generated, use ffmpeg to generate a `.mp4` video as:

     ffmpeg -r <fps> -f image2 -s <size> -i video-%05d.jpg -vcodec <video-codec> -crf <quality> -pix_fmt <pixel-format> output.mp4
     
e.g.

     ffmpeg -r 35 -f image2 -s 620x620 -i video-%05d.jpg -vcodec libx264 -crf 25 -pix_fmt yuv420p output.mp4

for other info on ffmeg usage, check this [link](http://hamelot.io/visualization/using-ffmpeg-to-convert-a-set-of-images-into-a-video/)

# Authors

* Giorgio Grisetti
